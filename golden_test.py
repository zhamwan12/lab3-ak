import contextlib
import io
import logging
import os
import tempfile
import unittest

import pytest

import translator
from control_unit import main


@pytest.mark.golden_test("test/*.yml")
def test_whole_by_golden(golden, caplog):
    caplog.set_level(logging.DEBUG)
    with tempfile.TemporaryDirectory() as tmpdirname:
        source = os.path.join(tmpdirname, "source.in")
        input_stream = os.path.join(tmpdirname, "input.txt")
        target = os.path.join(tmpdirname, "target.bin")

        with open(source, "w", encoding="utf-8") as file:
            file.write(golden["source"])
        with open(input_stream, "w", encoding="utf-8") as file:
            file.write(golden["input"])
        translator.main([source, target])
        with contextlib.redirect_stdout(io.StringIO()) as stdout:
            main([target, input_stream])

        assert stdout.getvalue() == golden.out["output"]