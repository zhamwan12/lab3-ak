# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=line-too-long

"""Модуль для тестирования основного функционала
"""

import unittest

from control_unit import ControlUnit
from data_path import DataPath
from isa import Signal, HaltProgram, SegmentOverflow


class TestTranslator(unittest.TestCase):

    def test_segmentation_overflow(self):
        swap_instruction = ['0000000011']
        instructions = swap_instruction * 100
        start_address = 0
        arg_words = ['Hello', 'World!']
        input_tokens = ['']
        data_path = DataPath(input_tokens, instructions, start_address, arg_words)
        data_path.stack = ['1', '2', '3']
        my_control_unit = ControlUnit(data_path)
        my_control_unit.start()
        self.assertRaises(SegmentOverflow)
        self.assertRaises(HaltProgram)

    def test_add_mod(self):
        data_path = DataPath([], [], 0, [])
        data_path.stack = ['5', '10']
        data_path.handle_signal(Signal.ADD)
        self.assertEqual(int(data_path.stack[0]), 15)

        data_path.stack = ['-10', '10']
        data_path.handle_signal(Signal.ADD)
        self.assertEqual(int(data_path.stack[0]), 0)

        data_path.stack = ['16', '4']
        data_path.handle_signal(Signal.MOD)
        self.assertEqual(int(data_path.stack[0]), 4)

        data_path.stack = ['4', '16']
        data_path.handle_signal(Signal.MOD)
        self.assertEqual(int(data_path.stack[0]), 16)

    def test_swap(self):
        data_path = DataPath([], [], 0, [])
        data_path.stack = ['5', '10']
        data_path.handle_signal(Signal.SWAP)
        self.assertEqual(data_path.stack, ['5', '10', '10', '5'])

    def test_check_not_zero(self):
        data_path = DataPath([], [], 0, [])
        data_path.stack = ['5', '10', '0']
        data_path.stack_pointer = 2
        data_path.handle_signal(Signal.CHECK_NOT_ZERO)
        self.assertEqual(data_path.stack[data_path.stack_pointer], 0)

        data_path.stack = ['5', '10', 0]
        data_path.stack_pointer = 2
        data_path.handle_signal(Signal.CHECK_NOT_ZERO)
        self.assertEqual(data_path.stack[data_path.stack_pointer], 0)

        data_path.stack = ['5', '10']
        data_path.stack_pointer = 1
        data_path.handle_signal(Signal.CHECK_NOT_ZERO)
        self.assertEqual(data_path.stack[data_path.stack_pointer], 1)

        data_path.stack = []
        data_path.stack_pointer = -1
        data_path.handle_signal(Signal.CHECK_NOT_ZERO)
        self.assertEqual(data_path.stack[data_path.stack_pointer], 1)

        data_path.stack = ['5', '10', 'Hello']
        data_path.stack_pointer = 2
        data_path.handle_signal(Signal.CHECK_NOT_ZERO)
        self.assertEqual(data_path.stack[data_path.stack_pointer], 1)

    def test_not_last(self):
        data_path = DataPath([], [], 0, [])
        data_path.stack = ['5', '10', '0']
        data_path.handle_signal(Signal.CHECK_NOT_LAST)
        self.assertEqual(data_path.stack[3], 1)

        data_path.stack = ['Hi']
        data_path.handle_signal(Signal.CHECK_NOT_LAST)
        self.assertEqual(data_path.stack[1], 0)

    def test_one(self):
        data_path = DataPath([], [], 0, [])
        data_path.stack = ['5', '10', '1']
        data_path.stack_pointer = 2
        data_path.handle_signal(Signal.CHECK_ONE)
        self.assertEqual(data_path.stack[3], 1)

        data_path.stack = ['5', '10', 1]
        data_path.stack_pointer = 2
        data_path.handle_signal(Signal.CHECK_ONE)
        self.assertEqual(data_path.stack[3], 1)

        data_path.stack = ['5', '10', '1Hello']
        data_path.stack_pointer = 2
        data_path.handle_signal(Signal.CHECK_ONE)
        self.assertEqual(data_path.stack[3], 1)

    def test_read_write(self):
        data_path = DataPath([], [], 0, [])

        data_path.input_tokens = ['A', 'A', 'A']
        data_path.handle_signal(Signal.WRITE)
        self.assertEqual(data_path.stack, ['AAA'])

        data_path.input_tokens = ['A', 'A', 'A', ' ', 'A']
        data_path.stack = []
        data_path.handle_signal(Signal.WRITE)
        self.assertEqual(data_path.stack, ['AAA'])
        data_path.handle_signal(Signal.WRITE)
        self.assertEqual(data_path.stack, ['AAA', 'A'])

        data_path.input_tokens = ['A', 'A', 'A', ' ', 'A', '1', '2', ' ', '9']
        data_path.stack = []
        data_path.handle_signal(Signal.WRITE)
        data_path.handle_signal(Signal.WRITE)
        data_path.handle_signal(Signal.WRITE)
        self.assertEqual(data_path.stack, ['AAA', 'A12', '9'])

        data_path.handle_signal(Signal.READ)
        self.assertEqual(data_path.stack, ['AAA', 'A12'])
        self.assertEqual(data_path.output_tokens, ['9', ' '])

        data_path.handle_signal(Signal.READ)
        self.assertEqual(data_path.stack, ['AAA'])
        self.assertEqual(data_path.output_tokens, ['9', ' ', 'A', '1', '2', ' '])
