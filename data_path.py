#!/usr/bin/python3
# pylint: disable=missing-function-docstring
# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-branches

"""Модуль, где находится Data Path
"""
import logging

from isa import Signal, InputTokensEnd, SegmentOverflow, Offsets


class DataPath:
    """ Тракт данных (пассивный), получает и обрабатывает сигналы от CU включая:
        ввод/вывод, память и арифметику.
    """

    def __init__(self, input_tokens, instructions, start_address, ro_data):
        assert Offsets.MEMORY_GENERAL_SIZE - Offsets.RO_DATA_OFFSET >= len(instructions), \
            "Памяти команд недостаточно для хранения программы"

        self.memory = instructions + [0] * (Offsets.RO_DATA_OFFSET - len(instructions))
        assert len(ro_data) < Offsets.DATA_OFFSET - Offsets.RO_DATA_OFFSET, \
            "Передан больший объем данных rodata, чем максимальный возможный (max 50)"
        if ro_data:
            self.memory += ro_data + \
                [0] * (Offsets.MEMORY_GENERAL_SIZE - Offsets.RO_DATA_OFFSET - len(ro_data))
        else:
            self.memory += [0] * (Offsets.MEMORY_GENERAL_SIZE - Offsets.RO_DATA_OFFSET)

        self.stack = []
        self.return_stack = []
        self.stack_pointer = -1

        assert start_address < Offsets.RO_DATA_OFFSET, \
            "Точка входа в программу находится все сегмента кода"
        self.program_counter = start_address

        self.input_tokens = input_tokens
        self.output_tokens = []

    def latch_pc(self):
        self.program_counter += 1
        if self.program_counter >= Offsets.RO_DATA_OFFSET:
            raise SegmentOverflow()

    def pop_stack(self):
        self.stack.pop()
        self.stack_pointer -= 1

    def push_end_to_stack(self):
        self.stack.append(0)
        self.stack_pointer += 1

    def get_next_instruction(self):
        return self.memory[self.program_counter - 1]

    def handle_write_signal(self):
        if self.input_tokens:
            word = []
            while True:
                if self.input_tokens:
                    symbol = self.input_tokens.pop(0)
                    if not symbol == ' ':
                        word.append(symbol)
                        logging.info('input: %s', symbol)
                    else:
                        assert len(word) <= 8, \
                            "Слишком длинное слово (max 64 бит)"
                        self.stack.append(''.join(word))
                        self.stack_pointer += 1
                        logging.info('Word pushed: %s', ''.join(word))
                        break
                else:
                    assert len(word) <= 8, \
                        "Слишком длинное слово (max 64 бит)"
                    self.stack.append(''.join(word))
                    self.stack_pointer += 1
                    logging.info('Word pushed: %s', ''.join(word))
                    break
        else:
            logging.info('End of input tokens')
            raise InputTokensEnd

    def handle_read_signal(self):
        if self.stack:
            word = self.stack.pop()
            self.stack_pointer -= 1
            try:
                symbols = list(word)
                for symbol in symbols:
                    logging.info('Output symbols: %s << %s',
                                 repr(''.join(self.output_tokens)), symbol)
                    self.output_tokens.append(symbol)
                self.output_tokens.append(' ')
                logging.info('Output symbols: %s', repr(''.join(self.output_tokens)))
            except TypeError:
                self.handle_read_signal()

    def handle_signal(self, signal):
        if signal == Signal.WRITE:
            self.handle_write_signal()
        elif signal == Signal.WRITE_ARG:
            offset = int(self.stack.pop())
            self.stack.append(self.memory[Offsets.RO_DATA_OFFSET + offset])
            self.handle_read_signal()
        elif signal == Signal.READ:
            self.handle_read_signal()
        elif signal == Signal.CHECK_ONE:
            try:
                check_ans = int(int(self.stack[self.stack_pointer - 1]) != 1) if self.stack else 1
            except ValueError:
                check_ans = 1
            self.stack.append(check_ans)
            self.stack_pointer += 1
        elif signal == Signal.CHECK_NOT_ZERO:
            try:
                check_ans = int(int(self.stack[self.stack_pointer]) != 0) if self.stack else 1
            except ValueError:
                check_ans = 1
            self.stack.append(check_ans)
            self.stack_pointer += 1
        elif signal == Signal.CHECK_NOT_LAST:
            check_ans = 0 if len(self.stack) == 1 else 1
            self.stack.append(check_ans)
            self.stack_pointer += 1
        elif signal == Signal.SWAP:
            a = self.stack[-1]
            b = self.stack[-2]
            self.stack.append(a)
            self.stack.append(b)
            self.stack_pointer += 2
        elif signal == Signal.MOD:
            operand = int(self.stack.pop())
            main = int(self.stack.pop())
            self.stack.append(operand)
            self.stack.append(str(main % operand))
        elif signal == Signal.ADD:
            self.stack.append(str(int(self.stack.pop()) + int(self.stack.pop())))
            self.stack_pointer -= 1
        elif signal == Signal.DEL:
            a = int(self.stack.pop())
            b = int(self.stack.pop())
            self.stack.append(str(int(b // a)))
            self.stack_pointer -= 1
        elif signal == Signal.MUL:
            self.stack.append(str(int(self.stack.pop()) * int(self.stack.pop())))
            self.stack_pointer -= 1
