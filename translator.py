#!/usr/bin/python3
# pylint: disable=missing-function-docstring
# pylint: disable=too-many-branches
# pylint: disable=too-many-statements
# pylint: disable=too-many-locals

"""Модуль, который транслирует программу в бинарный машинный код
"""

import enum
import sys


from isa import Command, JUMP_MASK


class Codes(enum.IntEnum):
    """Коды, которые помогают провести бинарное кодирование
    """
    PUSH_ARG_CODE = 512
    JMP_CODE = 384
    JZ_CODE = 256
    CALL_CODE = 128
    RET_CODE = 9
    HALT_CODE = 0
    RO_DATA_START = 150



symbol2command = {
    "halt": Command.HALT,
    "+": Command.ADD,
    "%": Command.MOD,
    "swap": Command.SWAP,
    "ztop": Command.ZTOP,
    "last": Command.LAST,
    ".": Command.PRINT,
    "otop": Command.OTOP,
    "<<": Command.PUSH,
    ";": Command.RETURN,
    "/": Command.DEL,
    "*": Command.MUL,
    "<": Command.PUSH_ARG,
    ":": None,
    "repeat": None,
    "begin": None,
    "while": None,
}


def dec_to_bin(number):
    return format(number, '010b')


def translate(source):
    program_bin, mnemonics = [], []
    functions = {}
    with open(source, "r", encoding="utf-8") as source_file:
        code = source_file.readlines()
    offset, jmp_pos, jz_pos, ro_data_offset = -1, -1, -1, 0
    in_comment, in_function = False, False
    for line in code:
        words = line.split()
        for word, next_word in zip(words, words[1:] + ['']):
            if word == "(":
                in_comment = True
                continue
            if word == ")":
                in_comment = False
                continue
            if word == ":":
                assert not in_function, "Function inside function!"
                in_function = True
                continue
            if word == "begin":
                jmp_pos = offset + 1
                continue
            if in_comment or word == "\n" or word == "<":
                continue
            if word not in symbol2command and in_function:
                functions[word] = offset + 1
                continue
            offset += 1
            if word == "repeat":
                program_bin.append(dec_to_bin(Codes.JMP_CODE + jmp_pos))
                mnemonics.append("jmp " + str(jmp_pos))
                program_bin[jz_pos] = dec_to_bin(Codes.JZ_CODE + offset + 1)
                mnemonics[jz_pos] = "jz " + str(offset + 1)
                continue
            if word == "while":
                program_bin.append("")
                mnemonics.append("")
                jz_pos = offset
                continue
            if word == ";":
                in_function = False
                program_bin.append(dec_to_bin(Codes.RET_CODE))
                mnemonics.append("ret")
                continue
            if word not in symbol2command and not in_function and not next_word == '<':
                program_bin.append(dec_to_bin(Codes.CALL_CODE + functions[word]))
                mnemonics.append("call " + str(functions[word]))
                continue
            if word not in symbol2command and not in_function and next_word == '<':
                program_bin.append(dec_to_bin(Codes.PUSH_ARG_CODE + ro_data_offset))
                ro_data_offset += 1
                mnemonics.append("< " + word)
                continue
            program_bin.append(dec_to_bin(symbol2command.get(word).value))
            mnemonics.append(word)
    program_bin.append(dec_to_bin(Codes.HALT_CODE))
    mnemonics.append("halt")
    return program_bin, mnemonics


def write_to_bin(target, binary_program, mnemonics):
    with open(target, "w", encoding="utf-8") as target_file:
        for i, _ in enumerate(binary_program):
            target_file.write(str(binary_program[i]))
            target_file.write(' ')
            target_file.write(str(mnemonics[i]))
            target_file.write("\n")


def read_instructions(filename):
    with open(filename, "r", encoding="utf-8") as binary_file:
        lines = binary_file.readlines()
    instructions = []
    arg_words = []
    start_address = -1
    for i, line in enumerate(lines):
        parts = line.split()
        if int(parts[0], 2) & JUMP_MASK == Codes.PUSH_ARG_CODE:
            arg_words.append(parts[2])
        instructions.append(parts[0])
        if int(parts[0], 2) == Codes.RET_CODE:
            start_address = i + 1
    if start_address == -1:
        start_address = 0
    return instructions, start_address, arg_words


def main(args):
    assert len(args) == 2, \
        "Wrong arguments: translator.py <source_file> <target_file>"
    source, target = args
    binary_program, mnemonics = translate(source)
    write_to_bin(target, binary_program, mnemonics)


if __name__ == '__main__':
    main(sys.argv[1:])