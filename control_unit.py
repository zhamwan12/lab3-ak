#!/usr/bin/python3
# pylint: disable=missing-function-docstring
# pylint: disable=too-many-branches
# pylint: disable=line-too-long

"""Модуль, где идет запуск процессора и находится Control Unit
"""

import logging
import sys

from data_path import DataPath
from isa import Signal, Command, Word, JUMP_MASK, \
    INSTR_ADDRESS_MASK, HaltProgram, InputTokensEnd, SegmentOverflow, Offsets
from translator import read_instructions

logging.basicConfig(level=logging.INFO)


class ControlUnit:
    """Блок управления процессора. Выполняет декодирование инструкций и
        управляет состоянием процессора, включая обработку данных (DataPath).
        После декодирования посылает сигнал в DataPath
    """

    def __init__(self, data_path):
        self.data_path = data_path
        self._tick = 0

    def tick(self):
        self._tick += 1

    def get_tick(self):
        return self._tick

    def send_signal(self, signal):
        self.data_path.handle_signal(signal)

    def start(self):
        cur_instr = ''
        limit = 1000
        instr_counter = 0
        try:
            while True:
                assert limit > instr_counter, "Достигнут лимит инструкций, программа остановлена"
                cur_instr =\
                    self.data_path.memory[Offsets.INSTRUCTIONS_OFFSET + self.data_path.program_counter]
                self.command_cycle()
                self.custom_log(cur_instr, self.data_path.program_counter - 1)
                instr_counter += 1
        except HaltProgram:
            pass
        self.custom_log(cur_instr, self.data_path.program_counter - 1)
        return ''.join(self.data_path.output_tokens), self.get_tick()

    def command_cycle(self):
        try:
            self.data_path.latch_pc()
        except SegmentOverflow as segment_fault:
            logging.error("Программа пытается исполнить сегмент данных, аварийный останов")
            raise HaltProgram() from segment_fault
        decoded_instruction = int(self.data_path.get_next_instruction(), 2)
        if decoded_instruction == Command.HALT:
            raise HaltProgram()
        if decoded_instruction == Command.OTOP:
            self.send_signal(Signal.CHECK_ONE)
        elif decoded_instruction == Command.ZTOP:
            self.send_signal(Signal.CHECK_NOT_ZERO)
        elif decoded_instruction == Command.LAST:
            self.send_signal(Signal.CHECK_NOT_LAST)
        elif decoded_instruction & JUMP_MASK == Command.JZ:
            if not bool(self.data_path.stack[self.data_path.stack_pointer]):
                self.data_path.program_counter = Offsets.INSTRUCTIONS_OFFSET + \
                    int(decoded_instruction & INSTR_ADDRESS_MASK)
            self.data_path.pop_stack()
        elif decoded_instruction & JUMP_MASK == Command.JUMP:
            self.data_path.program_counter = Offsets.INSTRUCTIONS_OFFSET + \
                int(decoded_instruction & INSTR_ADDRESS_MASK)
        elif decoded_instruction & JUMP_MASK == Command.CALL:
            self.data_path.return_stack.append(self.data_path.program_counter)
            self.data_path.program_counter = Offsets.INSTRUCTIONS_OFFSET + \
                int(decoded_instruction & INSTR_ADDRESS_MASK)
        elif decoded_instruction & JUMP_MASK == Command.PUSH_ARG:
            self.data_path.stack.append(int(decoded_instruction & INSTR_ADDRESS_MASK))
            self.data_path.stack_pointer += 1
            self.tick()
            self.send_signal(Signal.WRITE_ARG)
            self.tick()  # доп. такт на чтение из памяти
        elif decoded_instruction == Command.PUSH:
            try:
                self.send_signal(Signal.WRITE)
            except InputTokensEnd:
                self.data_path.push_end_to_stack()
        elif decoded_instruction == Command.SWAP:
            self.send_signal(Signal.SWAP)
        elif decoded_instruction == Command.MUL:
            self.send_signal(Signal.MUL)
        elif decoded_instruction == Command.DEL:
            self.send_signal(Signal.DEL)
        elif decoded_instruction == Command.MOD:
            self.send_signal(Signal.MOD)
        elif decoded_instruction == Command.ADD:
            self.send_signal(Signal.ADD)
        elif decoded_instruction == Command.RETURN:
            self.data_path.program_counter = Offsets.INSTRUCTIONS_OFFSET + \
                self.data_path.return_stack.pop()
        elif decoded_instruction == Command.PRINT:
            self.send_signal(Signal.READ)
        self.tick()

    def __repr__(self):
        top_element = self.data_path.stack[self.data_path.stack_pointer] \
            if self.data_path.stack_pointer != -1 else '-'
        ret = self.data_path.return_stack[len(self.data_path.return_stack) - 1] \
            if self.data_path.return_stack else '-'
        state = f"{{TICK: {self._tick}, PC: {self.data_path.program_counter}," \
                f" STACK POINTER: {self.data_path.stack_pointer}," \
                f" TOP ELEMENT: {top_element}," \
                f" RETURN STACK TOP EL: {ret} }}"
        return f"{state}"

    def custom_log(self, current_instruction, mem_address):
        binary_instruction = current_instruction
        decoded_instruction = int(binary_instruction, 2)
        assert decoded_instruction >= 0, "Инструкция должна быть беззнаковым числом"
        assert decoded_instruction < 1024, "Инструкция должна быть десятибитной"
        if decoded_instruction <= Command.DEL.value:
            mnemonics = Command(decoded_instruction).name
            address = ''
            symbol = Word[mnemonics].value
        else:
            mnemonics = Command(int(decoded_instruction & JUMP_MASK)).name
            address = int(decoded_instruction & INSTR_ADDRESS_MASK)
            symbol = ''
        action = f"({binary_instruction}) {mnemonics} {address} '{symbol}' @ {mem_address})"
        logging.info('%s %s', action, self)
        logging.info('%s', self.data_path.stack)


def run_processor(instructions, start_address, input_tokens, arg_words):
    data_path = DataPath(input_tokens, instructions, start_address, arg_words)
    control_unit = ControlUnit(data_path)
    output, ticks = control_unit.start()
    print(output)
    print("ticks:", ticks)


def main(args):
    assert len(args) == 2 or len(args) == 1, \
        "Wrong arguments: control_unit.py <binary code file> [<file with data>]"

    bin_code_file = args[0]
    data_file = args[1] if len(args) == 2 else None
    instructions, start_address, arg_words = read_instructions(bin_code_file)
    input_tokens = []
    if data_file:
        with open(data_file, "r", encoding="utf-8") as file:
            data = file.read()
            for char in data:
                input_tokens.append(char)
    run_processor(instructions, start_address, input_tokens, arg_words)


if __name__ == '__main__':
    main(sys.argv[1:])
