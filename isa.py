#!/usr/bin/python3

"""Модуль, куда вынесены общие элементы для Contol Unit и Data Path
"""

import enum

JUMP_MASK = int("1110000000", 2)
INSTR_ADDRESS_MASK = int("0001111111", 2)


class Offsets(enum.IntEnum):
    """Используются для относительной адресации
    """
    INSTRUCTIONS_OFFSET = 0
    RO_DATA_OFFSET = 100
    DATA_OFFSET = 150
    MEMORY_GENERAL_SIZE = 200


class Signal(enum.IntEnum):
    """Сигналы, которые посылает CU в Data Path в зависимости от декодироанной команды
    """
    WRITE = 0
    CHECK_ONE = 1
    CHECK_NOT_ZERO = 2
    CHECK_NOT_LAST = 3
    SWAP = 4
    MOD = 5
    ADD = 6
    READ = 7
    MUL = 8
    DEL = 9
    WRITE_ARG = 10





class Command(enum.IntEnum):
    """Команды, которые могут прийти в CU
    """
    HALT = 0
    ADD = 1
    MOD = 2
    SWAP = 3
    ZTOP = 4
    LAST = 5
    PRINT = 6
    OTOP = 7
    PUSH = 8
    RETURN = 9
    MUL = 10
    DEL = 11
    CALL = 128
    JZ = 256
    JUMP = 384
    PUSH_ARG = 512


class Word(str, enum.Enum):
    """Сопоставление команд и слов, которые им соответствуют в коде
    """
    HALT = 'halt'
    ADD = '+'
    MOD = '%'
    SWAP = 'swap'
    ZTOP = 'ztop'
    LAST = 'last'
    PRINT = '.'
    OTOP = 'otop'
    PUSH = '<<'
    RETURN = ';'
    PUSH_ARG = '<'
    MUL = '*'
    DEL = '/'


class HaltProgram(Exception):
    """Вызывается для остановки процессора
    """


class InputTokensEnd(Exception):
    """Вызывается для остановки считывания символов с потока
    """


class SegmentOverflow(Exception):
    """Вызывается для остановки программы, если выходим за пределы сегмента
    """
